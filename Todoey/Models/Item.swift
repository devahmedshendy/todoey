//
//  Item.swift
//  Todoey
//
//  Created by  Ahmed Shendy on 1/23/19.
//  Copyright © 2019 Ahmed Shendy. All rights reserved.
//

import Foundation

//struct Item: Encodable, Decodable {
struct Item: Codable {
    let title: String?
    var done: Bool = false
    
    init(title: String) {
        self.title = title
    }
}
