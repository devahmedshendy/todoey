//
//  TodoListViewController.swift
//  Todoey
//
//  Created by  Ahmed Shendy on 1/23/19.
//  Copyright © 2019 Ahmed Shendy. All rights reserved.
//

import UIKit

class TodoListViewController: UITableViewController {
    
    //MARK: Properties
    
//    var items = ["Find Mike", "Buy Eggos", "Destroy Demogrogon"]
    var items = [Item]()
//    let defaults = UserDefaults.standard
    let dataFilePath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("Items.plist")

    //MARK: App Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.items.append(Item.init(title: "Fine Mike"))
        self.items.append(Item.init(title: "Buy Eggos"))
        self.items.append(Item.init(title: "Destroy Demogrogon"))
        
//        if let items = self.defaults.array(forKey: "TodoListArray") as? [Item] {
//            self.items = items
//        }
        self.loadItems()
    }

    //MARK: Tableview DataSource Methods
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ToDoItemCell", for: indexPath)
        
        cell.textLabel?.text = self.items[indexPath.row].title
        cell.accessoryType = items[indexPath.row].done == true ? .checkmark : .none
        
        return cell
    }
    
    //MARK: Tableview Delegate Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell = tableView.cellForRow(at: indexPath)!
        var selectedItem = self.items[indexPath.row]
        
        if selectedCell.accessoryType == .checkmark {
            selectedCell.accessoryType = .none
            selectedItem.done = false
        } else {
            selectedCell.accessoryType = .checkmark
            selectedItem.done = true
        }
        
        self.saveItems()
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK: Add New Items
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        var textField = UITextField()
        let alert = UIAlertController(title: "Add New Todoey Item", message: "", preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Add Item", style: .default) { (action) in
            // what will happen once the user clicks the "Add Item" button on our UIAlert
            
            let newItem = Item(title: textField.text ?? "---")
            
            self.items.append(newItem)
//            do {
//                try self.defaults.set(PropertyListEncoder().encode(self.items), forKey: "TodoListArray")
//            } catch {
//                fatalError("Error: \(error.localizedDescription)")
//            }
            
            self.saveItems()
            
            self.tableView.reloadData()
        }
        
        alert.addAction(alertAction)
        alert.addTextField { (alertTextField) in
            alertTextField.placeholder = "Type New Item"
            textField = alertTextField
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    //MARK: Helper Methods
    
    private func saveItems() {
        do {
            let encoder = PropertyListEncoder()
            let data = try encoder.encode(self.items)
            try data.write(to: self.dataFilePath!)
        } catch {
            print("Error encoding Items array, \(error)")
        }
    }
    
    private func loadItems() {
        if let data = try? Data(contentsOf: self.dataFilePath!) {
            let decoder = PropertyListDecoder()
            do {
                self.items = try decoder.decode([Item].self, from: data)
            } catch {
                print("Error decoding item array, \(error)")
            }
        }
    }
}

